'use strict'

const mongoose = require('mongoose')
const csv = require('csv-streamify')
const fs = require('fs')

mongoose.connect('mongodb://localhost/test', {useNewUrlParser: true})
const { insertProducts } = require('../lib/dataAccess/product')
const { getCsvPath } = require('./lib/args')
const Batch = require('../lib/batch')
const InsertBatchStream = require('./lib/stream/InsertBatchStream')
const parser = csv()

const BATCH_SIZE = 200

const main = () => {
    const path = getCsvPath()
    const batch = new Batch(BATCH_SIZE)
    const stream = new InsertBatchStream(batch)
    const pipeline = fs.createReadStream(path).pipe(parser).pipe(stream)
    pipeline.on('finish', async function () {
      if (!batch.isEmpty()) {
        insertProducts(batch.getValues()).then(() => mongoose.disconnect())
      } else {
        console.log('end')
        mongoose.disconnect()
      }
    })
    pipeline.on('error', function (e) {
      console.log('error = ', e)
    })
}

try {
  main()
} catch (err) {
  // logger
  console.log('err = ', err)
  mongoose.disconnect()
}

