const getCsvPath = () => {
  const path = process.argv[2]
  if (!path) {
    throw new Error('path to csv file is missing')
  }
  return path
}

module.exports = {
  getCsvPath
}
