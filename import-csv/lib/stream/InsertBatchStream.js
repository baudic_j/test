'use strict'

const stream = require('stream')

const { convertLineToMongoProduct } = require('../../../lib/dataAccess/product')
const { insertProducts } = require('../../../lib/dataAccess/product')

class InsertBatchStream extends stream.Transform {
  constructor(batch) {
    const options = {
      objectMode : true
    }
    super(options)
    this.batch = batch
    this.promises = []
    this.i = 0
  }

  _transform(chunck, encoding, cb) {
    if (this.i === 0) {
      this.i++
      cb()
    } else {
      const mongoProduct = convertLineToMongoProduct(chunck[0].split(';'))
      if (!this.batch.isFull()) {
        this.batch.addValue(mongoProduct)
        cb()
      } else {
        insertProducts(this.batch.getValues()).then(() => {
          this.batch.empty()
          this.batch.addValue(mongoProduct)
          cb()
        })
        .catch(err => console.log('insert error = ', err))
      }
    }
  }
}

module.exports = InsertBatchStream
