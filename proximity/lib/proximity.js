'use strict'

const { isEqual } = require('lodash')
const proximity = require('colour-proximity')
const stream = require('stream')

const { rgbToHex } = require('../lib/color')
const productAccess = require('../../lib/dataAccess/product')

const PROXIMITY_THRESHOLD = 5

class Proximity extends stream.Transform {
  constructor(refProdct, refColor) {
    const options = {
      objectMode : true
    }
    super(options)
    this.refProdct = refProdct
    this.refColor = refColor
  }

  _transform(chunck, encoding, cb) {
    productAccess.getMainColor(chunck.productId).then(res => {
      const productHexaColor = rgbToHex(res.red, res.green, res.blue)
      if (!isEqual(chunck.productId, this.refProdct.productId) && proximity.proximity(this.refColor, productHexaColor) < PROXIMITY_THRESHOLD) {
        productAccess.addRecommendation(this.refProdct, chunck.productId)
          .then(() => cb())
          .catch(err => cb(new Error(err)))
      } else {
        cb()
      }
    })
    .catch(err => cb(new Error(err)))
  }
}

module.exports = Proximity
