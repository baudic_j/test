'use strict'

const productAccess = require('../../lib/dataAccess/product')
const { rgbToHex } = require('../lib/color')
const Proximity = require('../lib/proximity')

const getRecommendationAccordingToColor = async (req, res, next) => {
  try {
    const productId = req.params.id
    if (!productId) {
      throw new error('ProductId is missing')
    }
    const refProdct = await productAccess.getProductById(productId)
    if (!refProdct) {
      throw new Error(`Product with id ${productId} does not exist`)
    }
    const refColor = refProdct.mainColor
    const hexaProductColor = rgbToHex(refColor.red, refColor.green, refColor.blue)
    const hasAlreadyRecommendation = await productAccess.hasRecommendationAccordingToColor(refProdct)
    if (!hasAlreadyRecommendation) {
      const cursor = productAccess.getProductsCursor()
      const stream = new Proximity(refProdct, hexaProductColor)
      const pipeline = cursor.pipe(stream)
      pipeline.on('finish', async function () {
        const recommendations = await productAccess.getRecommendation(productId)
        res.status(200).send(recommendations)
      })
      pipeline.on('error', function (e) {
        next(e)
      })
    } else {
      const recommendations = await productAccess.getRecommendation(productId)
      res.status(200).send(recommendations)
    }
  } catch (err) {
    next(err)
  }
}

module.exports = {
  getRecommendationAccordingToColor
}
