// Imports
const express = require('express')
const router = express.Router()
const colorController = require('../../controller/ColorController')

// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
  console.log('colorRouter ', Date.now())
  next()
});
router.get('/:id', (req, res, next) => colorController.getRecommendationAccordingToColor(req, res, next))

module.exports = router
