const express = require('express')
const app = express()
const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/test', {useNewUrlParser: true})

const colorRouter = require('./router/color')
app.use('/proximity', colorRouter)

app.use((err, req, res, next) => {
  // add error logger
  console.log('err = ', err)
  return res.status(500).send(`something wen't wrong`);
})


app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})
