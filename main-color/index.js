'use strict'

const { getProductCursor } = require('./lib/mongo')
const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/test', {useNewUrlParser: true})

const { getMainColorOfProduct } = require('./lib/product')
const ColorStream = require('./lib/stream/ColorStream')

const main = async () => {
  const insertProducts = []
  const productCursor = getProductCursor()
  const stream = new ColorStream()
  const pipeline = productCursor.pipe(stream)
  pipeline.on('finish', async function () {
    console.log('finish')
    // if (!batch.isEmpty()) {
    //   insertProducts(batch.getValues()).then(() => mongoose.disconnect())
    // } else {
    //   console.log('end')
    //   mongoose.disconnect()
    // }
  })
  pipeline.on('error', function (e) {
    console.log('error = ', e)
  })
  // productCursor.on('data', async function(product) {
  //   if (!product.mainColor) {
  //     try{
  //       product.mainColor = await getMainColorOfProduct(product)
  //       insertProducts.push(product.save())
  //     } catch(err) {
  //       console.log('err = ', err)
  //     }
    
  //   }
  // })
  // productCursor.on('close', function() {
  //   Promise.all(insertProducts).then(() => {
  //     console.log('end')
  //   })
  // })
}

try {
  main()
} catch (err) {
  // add logger
  console.log('err = ', err)
}
