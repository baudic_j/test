const vision = require('@google-cloud/vision')

const getMainColorOfProduct = async (Product) => {
  const client = new vision.ImageAnnotatorClient();
  const [result] = await client.imageProperties(`http:${Product.photo}`)
  const colors = result.imagePropertiesAnnotation.dominantColors.colors
  return colors[0].color
}

module.exports = {
  getMainColorOfProduct
}
