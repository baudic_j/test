const Product = require('../../../lib/model/product')

const getProductCursor = () => {
  return Product.find({}).cursor()
}

module.exports = {
  getProductCursor
}
