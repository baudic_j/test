'use strict'

const stream = require('stream')

const { getMainColorOfProduct } = require('../product')

class ColorStream extends stream.Transform {
  constructor() {
    const options = {
      objectMode : true
    }
    super(options)
  }

  _transform(chunck, encoding, cb) {
    if (!chunck.mainColor) {
      getMainColorOfProduct(chunck)
      .then(colorOfProduct => {
        chunck.mainColor = colorOfProduct
        chunck.save()
          .then(() => cb())
          .catch(err => `error while saving product with color, productId: ${chunck._id}, error : ${err}`)
      })
      .catch(err => console.log(`err when get main color for produt ${chunck._id} : `, err))
    } else {
      cb()
    }
  }
}

module.exports = ColorStream
