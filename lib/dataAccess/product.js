'use strict'
const Product = require('../../lib/model/product')

const hasRecommendationAccordingToColor = product => {
  return product.proximity && product.proximity.length
}

const getProductById = (productId) => {
  return Product.findOne({productId})
}

const getProductsCursor = () => {
  return Product.find({}).cursor()
}

const getMainColor = async (productId) => {
  const product = await Product.findOne({productId})
  return product.mainColor
}

const addRecommendation = (Product, recommendationId) => {
  Product.proximity.push(recommendationId)
  return Product.save()
}

const getRecommendation = async (productId) => {
  const product = await getProductById(productId)
  const recommendations = product.proximity
  return recommendations
}

const insertProducts = products => {
  return Product.insertMany(products)
}

const convertLineToMongoProduct = line => {
  const product = new Product({
    productId: line[0],
    title: line[1],
    gender_id: line[2],
    composition: line[3],
    sleeve: line[4],
    photo: line[5],
    url: line[6]
  })
  return product
}

module.exports = {
  getMainColor,
  hasRecommendationAccordingToColor,
  getProductsCursor,
  addRecommendation,
  getProductById,
  getRecommendation,
  insertProducts,
  convertLineToMongoProduct
}