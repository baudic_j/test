const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ProductSchema = new Schema({
    productId:  String,
    title: String,
    gender_id: String,
    composition: String,
    sleeve: String,
    photo: String,
    url: String,
    mainColor: Object,
    proximity: [String],
})

ProductSchema.index({ productId: 1 })

module.exports = mongoose.model('Product', ProductSchema)
