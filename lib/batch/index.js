class Batch {
  constructor(size = 200) {
    this.size = size
    this.batch = []
  }

  addValue(val) {
    this.batch.push(val)
  }

  isFull() {
    return this.batch.length === this.size
  }

  empty() {
    this.batch = []
  }

  isEmpty() {
    return this.batch.length === 0
  }

  getValues() {
    return this.batch
  }
}

module.exports = Batch
